#pragma once
#include <cstddef>
#include <array>
#include <concepts>
#include <type_traits>

#define HZ_FWD(x) static_cast<decltype(x)&&>(x)

namespace Hz
{
	/**
	 * @brief A customisation pointer, providing the length of some statically sized container.
	 * 
	 * @note This is a customisation point for the user.
	 *
	 * @tparam T The type of the container, specialize on this type.
	 */
	template <typename T>
	struct NStaticallyKnownSize;
	/**
	 * @brief A specialization for ordinary arrays.
	 * 
	 * @tparam T The type of the elements in the array.
	 * @tparam N The length of the array.
	 */
	template <typename T, std::size_t N>
	struct NStaticallyKnownSize<T[N]>
	{
		static constexpr auto value = N; ///< The length of the container.
	};
	/**
	 * @brief A specialization for ordinary arrays.
	 * 
	 * @tparam T The type of the elements in the array.
	 * @tparam N The length of the array.
	 */
	template <typename T, std::size_t N>
	struct NStaticallyKnownSize<T(*)[N]>
	{
		static constexpr auto value = N; ///< The length of the container.
	};
	/**
	 * @brief A specialization for c++ arrays.
	 * 
	 * @tparam T The type of the elements in the array.
	 * @tparam N The length of the array.
	 */
	template <typename T, std::size_t N>
	struct NStaticallyKnownSize<std::array<T, N>>
	{
		static constexpr auto value = N; ///< The length of the container.
	};
	/**
	 * @brief Check if a type is registered as a statically known size.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept IsNStaticallyKnownSize = requires
	{
		{ NStaticallyKnownSize<std::remove_cvref_t<T>>::value } -> std::convertible_to<std::size_t>;
	};
	/**
	 * @brief Check if a type is an integral.
	 * 
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Integral = std::is_integral_v<std::remove_cvref_t<T>>;
	/**
	 * @brief A tag type to indicate that any pointer will be accepted for the concept `ConvertibleToPointer`.
	 */
	struct ConvertibleToAnyPointer{};
	/**
	 * @brief Check if some type `T` is convertible to the pointer `Obj`.
	 * 
	 * @tparam T The type to check.
	 * @tparam Obj The type that `T` must be convertible to.
	 */
	template <typename T, typename Obj>
	concept ConvertibleToPointer = ( 
		std::is_pointer_v<T> and
		std::same_as<Obj, ConvertibleToAnyPointer> or
		std::convertible_to<T, Obj>
	);
	/**
	 * @brief Some wrappers for standard library functions.
	 */
	namespace Std
	{
		using std::data;
		using std::size;

		template<typename Obj, typename T>
		concept HasData = requires( Obj && obj )
		{
			{ data( HZ_FWD(obj) ) } -> ConvertibleToPointer<T>;
		};

		template<typename Obj>
		concept HasSize = requires( Obj && obj )
		{
			{ size( HZ_FWD(obj) ) } -> Integral;
		};

		template <typename Obj>
		[[nodiscard]] constexpr auto Data( Obj && obj )
		{
			return data( HZ_FWD(obj) );
		}

		template <typename Obj>
		[[nodiscard]] constexpr auto Size( Obj && obj )
		{
			return size( HZ_FWD(obj) );
		}
	}
	/**
	 * @brief Check if some object has a member function `.Data()`.
	 * 
	 * @tparam Obj The object ot check.
	 * @tparam T The type returned by `.Data()` (must be a pointer).
	 */
	template<typename Obj, typename T>
	concept HasDataMemberFn = requires(Obj obj)
	{
		{ obj.Data() } -> ConvertibleToPointer<T>;
	};
	/**
	 * @brief Check if some type has support for the `data(obj)` method.
	 * 
	 * @tparam Obj The type of object to check.
	 * @tparam T The type of the result of the data method.
	 */
	template <typename Obj, typename T>
	concept HasData = Std::HasData<Obj&&, T> or HasDataMemberFn<Obj&&, T>;
	/**
	 * @brief Checks if some type is both of a known size and the `data(obj)` method returns `T`.
	 * 
	 * @tparam Obj The object type to check.
	 * @tparam T The type of the result of `data(obj)`.
	 */
	template <typename Obj, typename T>
	concept HasStaticallyKnownSizeAndPointsTo = IsNStaticallyKnownSize<Obj> and HasData<Obj, T>;
	static_assert( HasStaticallyKnownSizeAndPointsTo<const int ( & )[2], const int *> );
	/**
	 * @brief Get the size of some fixed known size object.
	 * 
	 * @tparam T The type to check.
	 */
	template <IsNStaticallyKnownSize T>
	inline constexpr auto statically_known_size_v = NStaticallyKnownSize<std::remove_cvref_t<T>>::value;
	/**
	 * @brief Check if all `Ns` in `Ns...` have the same value.
	 * 
	 * @tparam Ns The indexes to check.
	 */
	template <std::size_t... Ns>
	inline constexpr auto all_equal_v = false;
	/**
	 * @brief Check if all `Ns` in `Ns...` have the same value.
	 * 
	 * @tparam Ns The indexes to check.
	 */
	template <std::size_t F, std::size_t... Ns>
		requires( ( F == Ns ) and ... )
	inline constexpr auto all_equal_v<F, Ns...> = true;
	static_assert( all_equal_v<1> );
	static_assert( all_equal_v<1, 1, 1> );
	static_assert( not all_equal_v<> );
	static_assert( not all_equal_v<1, 2> );
	static_assert( not all_equal_v<1, 2, 3> );
	static_assert( not all_equal_v<1, 2, 1> );
	/**
	 * @brief Check if some object has a member function `.Size()` that returns an integral.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept HasSizeMemberFn = requires(T obj)
	{
		{ obj.Size() } -> Integral;
	};
	/**
	 * @brief Check if some object has either `size(obj)` or `obj.Size()`.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept HasSize = ::Hz::Std::HasSize<T> or HasSizeMemberFn<T> or IsNStaticallyKnownSize<T>;
	/**
	 * @brief Get the result of calling `.Data(obj)` or `data(obj). 
	 * 
	 * @tparam Ptr The pointer expected from the function call (If its unknown, provide `ConvertibleToAnyPointer`).
	 * @tparam Obj The object type.
	 * @param obj The object.
	 * @return A pointer to the element.
	 */
	template <typename Ptr, HasData<Ptr> Obj>
	[[nodiscard]] constexpr auto * Data(Obj && obj) noexcept
	{
		if constexpr (::Hz::Std::HasData<Obj, Ptr>)
		{
			return Std::Data(obj);
		}
		else if constexpr (HasDataMemberFn<Obj, Ptr>)
		{
			return obj.Data();
		}
	}
	/**
	 * @brief The number of elements in a container, as reported by `obj.Size()` or `size(obj)`.
	 * 
	 * @tparam T The type of the object.
	 * @param obj The object.
	 * @return The number of elements.
	 */
	template <HasSize T>
	[[nodiscard]] constexpr auto Size(T && obj) noexcept -> std::size_t
	{
		if constexpr (::Hz::Std::HasSize<T>)
		{
			return Std::Size(obj);
		}
		else if constexpr (HasSizeMemberFn<T>)
		{
			return obj.Size();
		}
		else if constexpr (IsNStaticallyKnownSize<T>)
		{
			return NStaticallyKnownSize<std::remove_cvref_t<T>>::value;
		}
	}
	/**
	 * @brief Checks if some type is both of a known size and the `data(obj)` method returns `T`.
	 * 
	 * @tparam Obj The object type to check.
	 * @tparam T The type of the result of `data(obj)`.
	 */
	template <typename Obj, typename T>
	concept HasSizeAndPointsTo = HasSize<Obj> and HasData<Obj, T>;
}