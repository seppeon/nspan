#pragma once
#include "Hz/NMeta.hpp"
#include <array>
#include <iterator>
#include <type_traits>

namespace Hz
{
	/**
	 * @brief A single column span.
	 * 
	 * @tparam T The types of elements in the span.
	 */
	template <typename T>
		requires( not std::is_reference_v<T> )
	class Span
	{
		using size_type = size_t;
		using value_type = T;
		using pointer = T *;
		using reference = T &;
		using difference_type = std::make_signed_t<std::size_t>;

		static constexpr size_type npos = size_type( -1 );
	public:
		/**
		 * @brief Default construct the span, it'll be empty.
		 */
		constexpr Span() = default;
		/**
		 * @brief Move construct the span.
		 *
		 * @param data The data to move from.
		 */
		constexpr Span( Span && data ) = default;
		/**
		 * @brief Copy construct the span.
		 *
		 * @param data The data to copy from.
		 */
		constexpr Span( Span const & data ) = default;
		/**
		 * @brief Move assign the span.
		 *
		 * @param data The data to move from.
		 * @return A reference to `this` span.
		 */
		Span & operator=( Span && data ) noexcept = default;
		/**
		 * @brief Copy assign the span.
		 * 
		 * @param data The data to copy from.
		 * @return A reference to `this` span.
		 */
		Span & operator=( Span const & data ) noexcept = default;
		/**
		 * @brief Construct the span from a length and a pointer.
		 * 
		 * @param length The number of elements in the span.
		 * @param ptr Pointer to the first element of the span.
		 */
		constexpr Span( size_type length, T * ptr ) noexcept
			: m_length{ length }
			, m_ptr{ ptr }
		{}
		/**
		 * @brief Construct the span from a container with a known size.
		 * 
		 * @tparam Ts The types of the known size container.
		 * @param arg The known size container.
		 */
		template <HasSizeAndPointsTo<T *> Ts>
		constexpr Span( Ts && arg ) noexcept
			: m_ptr{ Hz::Data<pointer>(HZ_FWD(arg)) }
			, m_length{ Hz::Size(HZ_FWD(arg)) }
		{}
		using iterator = T *;
		/**
		 * @brief Get the begin iterator of the span.
		 * 
		 * @return The begin iterator.
		 */
		constexpr auto begin() const noexcept -> iterator
		{
			return m_ptr;
		}
		/**
		 * @brief Get the end iterator of the span.
		 * 
		 * @return The end iterator.
		 */
		constexpr auto end() const noexcept -> iterator
		{
			return m_ptr + m_length;
		}
		/**
		 * @brief Checks if a span has length elements.
		 * 
		 * @return The number of elements in the span.
		 */
		[[nodiscard]] constexpr auto Data() const noexcept -> pointer
		{
			return m_ptr;
		}
		/**
		 * @brief Checks if a span has length elements.
		 * 
		 * @return The number of elements in the span.
		 */
		[[nodiscard]] constexpr auto Size() const noexcept -> size_type
		{
			return m_length;
		}
		/**
		 * @brief Checks if the span has a length of 0.
		 * 
		 * @retval true The span is empty.
		 * @retval false The span has elements.
		 */
		[[nodiscard]] constexpr bool Empty() const noexcept
		{
			return m_length == 0;
		}
		/**
		 * @brief Access the `index`th element of the span.
		 *
		 * @warning No bounds checking is currently present in this function. 
		 *
		 * @param index The element to access.
		 * @return The requested element.
		 */
		[[nodiscard]] constexpr reference operator[]( size_type index ) const noexcept
		{
			return m_ptr[index];
		}
		/**
		 * @brief Equal, as normal.
		 *
		 * Compare all elements for equality, short circuiting on the first mismatch. 
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs == rhs
		 */
		[[nodiscard]] friend constexpr bool operator==( Span const & lhs, Span const & rhs ) noexcept
		{
			if ( lhs.Size() != rhs.Size() ) return false;
			for ( std::size_t i = 0; i < lhs.m_length; ++i )
			{
				if ( lhs[i] != rhs[i] )
				{
					return false;
				}
			}
			return true;
		}
		/**
		 * @brief Less, as normal.
		 *
		 * This will lexicographically compare the elements of the two spans.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs < rhs
		 */
		[[nodiscard]] friend constexpr bool operator<( Span const & lhs, Span const & rhs ) noexcept
		{
			return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end() );
		}
		/**
		 * @brief Not equal, as normal.
		 *
		 * See @ref Span::operator== For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs != rhs
		 */
		[[nodiscard]] friend constexpr bool operator!=( Span const & lhs, Span const & rhs ) noexcept
		{
			return not( lhs == rhs );
		}
		/**
		 * @brief Greater, as normal.
		 *
		 * See @ref Span::operator< For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs > rhs
		 */
		[[nodiscard]] friend constexpr bool operator>( Span const & lhs, Span const & rhs ) noexcept
		{
			return rhs < lhs;
		}
		/**
		 * @brief Less or equal, as normal.
		 *
		 * See @ref Span::operator< For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs <= rhs
		 */
		[[nodiscard]] friend constexpr bool operator<=( Span const & lhs, Span const & rhs ) noexcept
		{
			return not( lhs > rhs );
		}
		/**
		 * @brief Greater or equal, as normal.
		 *
		 * See @ref Span::operator< For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs >= rhs
		 */
		[[nodiscard]] friend constexpr bool operator>=( Span const & lhs, Span const & rhs ) noexcept
		{
			return not( rhs < lhs );
		}
		/**
		 * @brief Get a reference to the first element of this span.
		 * 
		 * @return The first element.
		 */
		[[nodiscard]] constexpr reference Front() const noexcept
		{
			return *m_ptr;
		}
		/**
		 * @brief Get a reference to the last element of this span.
		 * 
		 * @return The last element.
		 */
		[[nodiscard]] constexpr reference Back() const noexcept
		{
			return m_ptr[m_length - 1];
		}
		/**
		 * @brief Produce a new span with `count` elements removed from the front.
		 *
		 * @code
		 * auto result = span.PopFront();
		 * @endcode
		 *
		 * `span` remains unchanged, result contains the span after the operation, `result` contains:
		 * @code
		 * span = [1, 2]
		 * result = [2]
		 * @endcode
		 *
		 * @note Any number of elements can be popped from the span, provide the number of elements to pop as an argument to `.PopFront(arg)`.
		 * 
		 * @param count The number of elements to remove.
		 * @return The new span with the elements removed.
		 */
		[[nodiscard]] constexpr Span PopFront( size_type count = 1 ) const noexcept
		{
			auto c{ std::min( m_length, count ) };
			return { &m_ptr[c], m_length - c };
		}
		/**
		 * @brief Produce a new span with `count` elements removed from the back.
		 *
		 * @code
		 * auto result = span.PopBack();
		 * @endcode
		 *
		 * `span` remains unchanged, result contains the span after the operation, `result` contains:
		 * @code
		 * span = [1, 2]
		 * result = [1]
		 * @endcode
		 *
		 * @note Any number of elements can be popped from the span, provide the number of elements to pop as an argument to `.PopBack(arg)`.
		 * 
		 * @param count The number of elements to remove.
		 * @return The new span with the elements removed.
		 */
		[[nodiscard]] constexpr Span PopBack( size_type count = 1 ) const noexcept
		{
			auto const len = m_length - std::min(count, m_length);
			return { m_ptr, len };
		}
		/**
		 * @brief Produce a new span representing the overlapping regions between the requested range and the range represented by this span (ie. [0, length]).
		 *
		 * @note If the requested index is out of range, an empty span will be returned.
		 * @note If the length is greater than the size of this span, the result will be clamped to avaliable characters.
		 *
		 * @param index The index to start the subspan.
		 * @param length The length of the subspan.
		 * @return The requested subspan. 
		 */
		[[nodiscard]] constexpr Span Subspan( size_type index, size_type length = npos ) const noexcept
		{
			auto const sta = std::min( index, m_length );
			auto const len = std::min( length, m_length - sta );
			return { &m_ptr[sta], len };
		}
		/**
		 * @brief Produce a span representing the first `length` elements of this span.
		 * 
		 * @param length The number of elements in the span.
		 * @return The produced span with the first `length` elements of `this` span.
		 */
		[[nodiscard]] constexpr Span First( size_type length ) const noexcept
		{
			return { m_ptr, std::min( length, m_length ) };
		}
		/**
		 * @brief Produce a span representing the last `length` elements of this span.
		 * 
		 * @param length The number of elements in the span.
		 * @return The produced span with the last `length` elements of `this` span.
		 */
		[[nodiscard]] constexpr Span Last( size_type length ) const noexcept
		{
			auto const len = std::min( length, m_length );
			auto const sta =  m_length - len;
			return { &m_ptr[sta], len };
		}
		/**
		 * @brief The type returned from split operations.
		 */
		struct SplitResult
		{
			Span lhs;
			Span rhs;
		};
		/**
		 * @brief Produce a pair of spans, representing the portions of the span on either side of the `index` requested.
		 * 
		 * Conceptually:
		 * @code
		 * abcde
		 * @endcode
		 *
		 * Split at index 2 will produce:
		 * @code
		 * lhs = ab
		 * rhs = cde
		 * @endcode
		 *
		 * @note If the split index is equal or beyond the range, the `rhs` half of the operation will be empty.
		 *
		 * @param index The index to split `this` span.
		 * @return The produced pair of spans.
		 */
		[[nodiscard]] constexpr SplitResult Split( size_type index ) const noexcept
		{
			auto const i = std::min( index, m_length );
			return { { m_ptr, i }, { &m_ptr[i], m_length - i } };
		}
	private:
		constexpr Span(pointer ptrs, std::size_t len)
			: m_ptr(ptrs)
			, m_length(len)
		{}

		std::size_t m_length;
		pointer m_ptr;
	};

	template <typename T>
	Span( std::size_t length, T * ptr ) -> Span<T>;

	template <HasSizeAndPointsTo<ConvertibleToAnyPointer> Arg>
	Span( Arg && arg ) -> Span<std::remove_pointer_t<decltype(Hz::Data<ConvertibleToAnyPointer>(HZ_FWD(arg)))>>;
}