#pragma once
#include "Hz/NTup.hpp"
#include "Hz/NMeta.hpp"
#include <array>
#include <iterator>

namespace Hz
{
	/**
	 * @brief A multi-column span.
	 * 
	 * @tparam Args... The types of elements in the span (in the order of the columns).
	 */
	template <typename... Args>
		requires( not( std::is_reference_v<Args> and ... ) )
	class NSpan
	{
		using size_type = size_t;
		using value_type = NTup<Args...>;
		using pointer = NTup<Args *...>;
		using reference = NTup<Args &...>;
		using difference_type = std::make_signed_t<std::size_t>;

		static constexpr size_type npos = size_type( -1 );
	public:
		/**
		 * @brief A random access iterator, as per `std::random_access_iterator<T>`.
		 */
		struct iterator
		{
			using value_type = NTup<Args...>;
			using pointer = NTup<Args *...>;
			using reference = NTup<Args &...>;
			using iterator_category = std::random_access_iterator_tag;
			using difference_type = std::make_signed_t<std::size_t>;

			[[nodiscard]] constexpr auto operator*() const noexcept -> reference
			{
				return ( *m_ptr )[m_index];
			}
			[[nodiscard]] constexpr auto operator[]( difference_type index ) const noexcept -> reference
			{
				return ( *m_ptr )[m_index + index];
			}
			constexpr auto operator++() noexcept -> iterator &
			{
				++m_index;
				return *this;
			}
			constexpr auto operator--() noexcept -> iterator &
			{
				--m_index;
				return *this;
			}
			constexpr auto operator++( int ) noexcept -> iterator
			{
				auto out = *this;
				++*this;
				return out;
			}
			constexpr auto operator--( int ) noexcept -> iterator
			{
				auto out = *this;
				--*this;
				return out;
			}
			constexpr auto operator+=( difference_type diff ) noexcept -> iterator &
			{
				m_index += diff;
				return *this;
			}
			constexpr auto operator-=( difference_type diff ) noexcept -> iterator &
			{
				m_index -= diff;
				return *this;
			}

			[[nodiscard]] friend constexpr auto operator+( iterator const & lhs, difference_type diff ) noexcept -> iterator
			{
				return iterator{ lhs.m_ptr, lhs.m_index + diff };
			}
			[[nodiscard]] friend constexpr auto operator+( difference_type diff, iterator const & rhs ) noexcept -> iterator
			{
				return iterator{ rhs.m_ptr, rhs.m_index + diff };
			}
			[[nodiscard]] friend constexpr auto operator-( iterator const & lhs, difference_type diff ) noexcept -> iterator
			{
				return iterator{ lhs.m_ptr, lhs.m_index - diff };
			}
			[[nodiscard]] friend constexpr auto operator-( iterator const & lhs, iterator const & rhs ) noexcept -> difference_type
			{
				return lhs.m_index - rhs.m_index;
			}
			[[nodiscard]] friend constexpr auto operator==( iterator const & lhs, iterator const & rhs ) noexcept -> bool
			{
				return lhs.m_index == rhs.m_index;
			}
			[[nodiscard]] friend constexpr auto operator!=( iterator const & lhs, iterator const & rhs ) noexcept -> bool
			{
				return lhs.m_index != rhs.m_index;
			}
			[[nodiscard]] friend constexpr auto operator<( iterator const & lhs, iterator const & rhs ) noexcept -> bool
			{
				return lhs.m_index < rhs.m_index;
			}
			[[nodiscard]] friend constexpr auto operator>( iterator const & lhs, iterator const & rhs ) noexcept -> bool
			{
				return lhs.m_index > rhs.m_index;
			}
			[[nodiscard]] friend constexpr auto operator<=( iterator const & lhs, iterator const & rhs ) noexcept -> bool
			{
				return lhs.m_index <= rhs.m_index;
			}
			[[nodiscard]] friend constexpr auto operator>=( iterator const & lhs, iterator const & rhs ) noexcept -> bool
			{
				return lhs.m_index >= rhs.m_index;
			}

			constexpr iterator() = default;
			constexpr iterator( pointer const * ptr, difference_type index )
				: m_ptr( ptr )
				, m_index( index )
			{
			}
		private:
			pointer const * m_ptr = nullptr;
			difference_type m_index = 0;
		};
		/**
		 * @brief Default construct the span, it'll be empty.
		 */
		constexpr NSpan() = default;
		/**
		 * @brief Move construct the span.
		 *
		 * @param data The data to move from.
		 */
		constexpr NSpan( NSpan && data ) = default;
		/**
		 * @brief Copy construct the span.
		 *
		 * @param data The data to copy from.
		 */
		constexpr NSpan( NSpan const & data ) = default;
		/**
		 * @brief Move assign the span.
		 *
		 * @param data The data to move from.
		 * @return A reference to `this` span.
		 */
		NSpan & operator=( NSpan && data ) noexcept = default;
		/**
		 * @brief Copy assign the span.
		 * 
		 * @param data The data to copy from.
		 * @return A reference to `this` span.
		 */
		NSpan & operator=( NSpan const & data ) noexcept = default;
		/**
		 * @brief Construct the span from a length and a set of pointers.
		 * 
		 * @param length The number of rows in the span.
		 * @param ptrs... Pointers to the elements of the first row.
		 */
		constexpr NSpan( size_type length, Args *... ptrs ) noexcept
			: m_length{ length }
			, m_ptrs{ ptrs... }
		{}
		/**
		 * @brief Construct the span from a set of containers with known sizes.
		 * 
		 * @tparam Ts... The types of the known size containers.
		 * @param args... The known size containers.
		 */
		template <typename... Ts>
			requires( (sizeof...( Ts ) == sizeof...( Args ) and ( HasStaticallyKnownSizeAndPointsTo<Ts, Args *> and ... ) and all_equal_v<statically_known_size_v<Ts>...>))
		constexpr NSpan( Ts &&... args ) noexcept
			: m_length{ statically_known_size_v<decltype( ( HZ_FWD(args), ... ) )> }
			, m_ptrs{ Hz::Data<Args *>( HZ_FWD(args) )... }
		{}
		/**
		 * @brief Get the begin iterator of the span.
		 * 
		 * @return The begin iterator.
		 */
		constexpr auto begin() const noexcept -> iterator
		{
			return iterator( std::addressof( m_ptrs ), 0 );
		}
		/**
		 * @brief Get the end iterator of the span.
		 * 
		 * @return The end iterator.
		 */
		constexpr auto end() const noexcept -> iterator
		{
			return iterator( std::addressof( m_ptrs ), m_length );
		}
		/**
		 * @brief The number of columns in the span.
		 * 
		 * @return The number of columns.
		 */
		[[nodiscard]] static constexpr auto Columns() noexcept -> size_type
		{
			return sizeof...( Args );
		}
		/**
		 * @brief Get a tuple of pointers, each representing a pointer at the start of each column.
		 * 
		 * @return The pointers.
		 */
		[[nodiscard]] constexpr auto Data() const noexcept -> pointer
		{
			return m_ptrs;
		}
		/**
		 * @brief Checks if a span has length elements.
		 * 
		 * @return The number of rows in the span.
		 */
		[[nodiscard]] constexpr auto Size() const noexcept -> size_type
		{
			return m_length;
		}
		/**
		 * @brief Checks if the span has a length of 0.
		 * 
		 * @retval true The span is empty.
		 * @retval false The span has rows.
		 */
		[[nodiscard]] constexpr bool Empty() const noexcept
		{
			return m_length == 0;
		}
		/**
		 * @brief Access the `index`th row of the span.
		 *
		 * @warning No bounds checking is currently present in this function. 
		 *
		 * @param index The row to access.
		 * @return The requested row.
		 */
		[[nodiscard]] constexpr reference operator[]( size_type index ) const noexcept
		{
			return m_ptrs[index];
		}
		/**
		 * @brief Equal, as normal.
		 *
		 * Compare all elements for equality, short circuiting on the first mismatch. 
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs == rhs
		 */
		[[nodiscard]] friend constexpr bool operator==( NSpan const & lhs, NSpan const & rhs ) noexcept
		{
			if ( lhs.Size() != rhs.Size() ) return false;
			return apply(
				[&]( auto const *... ls ) -> bool
				{
					return apply(
						[&]( auto const *... rs )
						{
							for ( std::size_t i = 0; i < lhs.m_length; ++i )
							{
								if ( not( ( ls[i] == rs[i] ) and ... ) ) { return false; }
							}
							return true;
						},
						rhs.m_ptrs );
				},
				lhs.m_ptrs
			);
		}
		/**
		 * @brief Less, as normal.
		 *
		 * This will lexicographically compare the rows of the two spans.
		 *
		 * For example, given:
		 * @code
		 * span0 = [
		 *  	[1, 2],
		 *  	['4', '5'],
		 *  	[true, false]
		 * ]
		 * span1 = [
		 *  	[3, 4],
		 *  	['5', '6'],
		 *  	[false, true]
		 * ]
		 * @endcode
		 *
		 * The first elements compared will be:
		 * @code
		 * [1, 2] < [3, 4]
		 * @endcode
		 * Each side of the above will also be lexicographically compared.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs < rhs
		 */
		[[nodiscard]] friend constexpr bool operator<( NSpan const & lhs, NSpan const & rhs ) noexcept
		{
			return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end() );
		}
		/**
		 * @brief Not equal, as normal.
		 *
		 * See @ref NSpan::operator== For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs != rhs
		 */
		[[nodiscard]] friend constexpr bool operator!=( NSpan const & lhs, NSpan const & rhs ) noexcept
		{
			return not( lhs == rhs );
		}
		/**
		 * @brief Greater, as normal.
		 *
		 * See @ref NSpan::operator< For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs > rhs
		 */
		[[nodiscard]] friend constexpr bool operator>( NSpan const & lhs, NSpan const & rhs ) noexcept
		{
			return rhs < lhs;
		}
		/**
		 * @brief Less or equal, as normal.
		 *
		 * See @ref NSpan::operator< For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs <= rhs
		 */
		[[nodiscard]] friend constexpr bool operator<=( NSpan const & lhs, NSpan const & rhs ) noexcept
		{
			return not( lhs > rhs );
		}
		/**
		 * @brief Greater or equal, as normal.
		 *
		 * See @ref NSpan::operator< For more information.
		 *
		 * @param lhs The left hand operand.
		 * @param rhs The right hand operand.
		 * @return lhs >= rhs
		 */
		[[nodiscard]] friend constexpr bool operator>=( NSpan const & lhs, NSpan const & rhs ) noexcept
		{
			return not( rhs < lhs );
		}
		/**
		 * @brief Get a reference to the first row of this span.
		 * 
		 * @return The first row.
		 */
		[[nodiscard]] constexpr reference Front() const noexcept
		{
			return *m_ptrs;
		}
		/**
		 * @brief Get a reference to the last row of this span.
		 * 
		 * @return The last row.
		 */
		[[nodiscard]] constexpr reference Back() const noexcept
		{
			return m_ptrs[m_length - 1];
		}
		/**
		 * @brief Produce a new span with `count` rows removed from the front.
		 *
		 * @code
		 * auto result = span.PopFront();
		 * @endcode
		 *
		 * `span` remains unchanged, result contains the span after the operation, `result` contains:
		 * @code
		 * span = [
		 *  	[1, 2],
		 *  	['4', '5'],
		 *  	[true, false]
		 * ]
		 *
		 * result = [
  		 *     [2],
  		 *     ['5'],
  		 *     [false]
		 * ]
		 * @endcode
		 *
		 * @note Any number of rows can be popped from the span, provide the number of rows to pop as an argument to `.PopFront(arg)`.
		 * 
		 * @param count The number of rows to remove.
		 * @return The new span with the rows removed.
		 */
		[[nodiscard]] constexpr NSpan PopFront( size_type count = 1 ) const noexcept
		{
			auto c{ std::min( m_length, count ) };
			return { &m_ptrs[c], m_length - c };
		}
		/**
		 * @brief Produce a new span with `count` rows removed from the back.
		 *
		 * @code
		 * auto result = span.PopBack();
		 * @endcode
		 *
		 * `span` remains unchanged, result contains the span after the operation, `result` contains:
		 * @code
		 * span = [
		 *  	[1, 2],
		 *  	['4', '5'],
		 *  	[true, false]
		 * ]
		 * 
		 * result = [
		 *  	[1],
		 *  	['4'],
		 *  	[true]
		 * ]
		 * @endcode
		 *
		 * @note Any number of rows can be popped from the span, provide the number of rows to pop as an argument to `.PopBack(arg)`.
		 * 
		 * @param count The number of rows to remove.
		 * @return The new span with the rows removed.
		 */
		[[nodiscard]] constexpr NSpan PopBack( size_type count = 1 ) const noexcept
		{
			auto const len = m_length - std::min(count, m_length);
			return { m_ptrs, len };
		}
		/**
		 * @brief Produce a new span representing the overlapping regions between the requested range and the range represented by this span (ie. [0, length]).
		 *
		 * @note If the requested index is out of range, an empty span will be returned.
		 * @note If the length is greater than the size of this span, the result will be clamped to avaliable characters.
		 *
		 * @param index The index to start the subspan.
		 * @param length The length of the subspan.
		 * @return The requested subspan. 
		 */
		[[nodiscard]] constexpr NSpan Subspan( size_type index, size_type length = npos ) const noexcept
		{
			auto const sta = std::min( index, m_length );
			auto const len = std::min( length, m_length - sta );
			return { &m_ptrs[sta], len };
		}
		/**
		 * @brief Produce a span representing the first `length` rows of this span.
		 * 
		 * @param length The number of rows in the span.
		 * @return The produced span with the first `length` rows of `this` span.
		 */
		[[nodiscard]] constexpr NSpan First( size_type length ) const noexcept
		{
			return { m_ptrs, std::min( length, m_length ) };
		}
		/**
		 * @brief Produce a span representing the last `length` rows of this span.
		 * 
		 * @param length The number of rows in the span.
		 * @return The produced span with the last `length` rows of `this` span.
		 */
		[[nodiscard]] constexpr NSpan Last( size_type length ) const noexcept
		{
			auto const len = std::min( length, m_length );
			auto const sta =  m_length - len;
			return { &m_ptrs[sta], len };
		}
		/**
		 * @brief The type returned from split operations.
		 */
		struct SplitResult
		{
			NSpan lhs;
			NSpan rhs;
		};
		/**
		 * @brief Produce a pair of spans, representing the portions of the span on either side of the `index` requested.
		 * 
		 * Conceptually:
		 * @code
		 * abcde
		 * @endcode
		 *
		 * Split at index 2 will produce:
		 * @code
		 * lhs = ab
		 * rhs = cde
		 * @endcode
		 *
		 * @note If the split index is equal or beyond the range, the `rhs` half of the operation will be empty.
		 *
		 * @param index The index to split `this` span.
		 * @return The produced pair of spans.
		 */
		[[nodiscard]] constexpr SplitResult Split( size_type index ) const noexcept
		{
			auto const i = std::min( index, m_length );
			return { { m_ptrs, i }, { &m_ptrs[i], m_length - i } };
		}
	private:
		constexpr NSpan(pointer ptrs, std::size_t len)
			: m_length(len)
			, m_ptrs(ptrs)
		{}

		std::size_t m_length;
		pointer m_ptrs;
	};

	template <typename... Args>
	NSpan( std::size_t length, Args *... ptrs ) -> NSpan<Args...>;

	template <typename... Ts>
		requires( ( HasStaticallyKnownSizeAndPointsTo<Ts, ConvertibleToAnyPointer> and ... ) and all_equal_v<statically_known_size_v<Ts>...>)
	NSpan( Ts &&... args ) -> NSpan<std::remove_pointer_t<decltype(Hz::Data<ConvertibleToAnyPointer>(HZ_FWD(args)))>...>;
}