\mainpage NSpan

The `Hz::NSpan` is an `N` column span. Allowing a view into contiguous sets of data. `Hz::NSpan` is part of the `Hz` library, and is contained within that namespace.

# Installation

To use the library:

1. Clone the repo and open the cloned directory:
```
git clone https://gitlab.com/seppeon/nspan.git
cd nspan
```

2. Create a `build` directory, and enter that directory:
```
mkdir build
cd build
```

3. Install the dependancies (`catch2`) using `conan`:
```
conan install .. --profile:build=mingw --profile:host=mingw --build=missing -s build_type=Release
```

4. This package is not yet in conan center, so to install it locally (for consumption use with conan) run:
```
conan create .. --profile:build=mingw --profile:host=mingw --build=missing -s build_type=Release
```

5. From any other `conan` project on your system, in the cmake lists:
```CMAKE
find_package(NSpan REQUIRED)
add_library(ExampleLibrary)
target_link_libraries(ExampleLibrary PUBLIC Hz::NSpan)
```
and add the dependancy to your `conanfile.py`, or `conanfile.txt`.

# Idea

The design of the `Hz::NSpan` is such that only through assignment can a mutation to the span be performed. This has several benefits, but the main benefit is that it is easier to reason about the state of a given object.

A contrived(for brevity) example of such a bug:

```CPP
void do_foo(std::filesystem::path p, std::string contents)
{
    do_bar(p.remove_filename());
    write_file(p, contents);
}
```

The intention here, to do some operation on the path without the filename, then write contents to the file specfied by the original value of `p`. That is not what this does, and this will be obvious most of the time, until it isn't. 

I found myself occasionally writing bugs when using `std::filesystem::path`, it has a mix of mutating and non-mutating operations. I'd find myself inadvertly mutating the object, a bug I would rather not make. This motivated the design of many of the `Hz` view objects.

# Creation

To instantiate an `Hz::NSpan`, you have two options:

1. Use pointers and a valid length:
```CPP
int a[2]{1, 2};
char b[5]{'4', '5', '6', '7', '8'};
bool c[3]{true, false, true};

auto const min_size = std::min(std::min(std::size(a), std::size(b)), std::size(c));
Hz::NSpan span(min_size, std::data(a), std::data(b), std::data(c));
```

2. When you have containers of a known and identical size, construct using the deduced size constructor:
```CPP
int a[2]{1, 2};
char b[2]{'4', '5'};
bool c[2]{true, false};
Hz::NSpan span(a, b, c);
```
A user of the library can add support for this constructor by customising the `Hz::NStaticallyKnownSize` object. This object must provide the `static constexpr` data member `value` which represents the object's length. The object that is customisable must support a `data(object)` method. 

# Iteration

The `Hz::NSpan` behaves very much like a normal span, with random access iterators and support for range based for loops.

```CPP
int as[2]{1, 2};
char bs[2]{'4', '5'};
bool cs[2]{true, false};
Hz::NSpan span(as, bs, cs);

for (auto [a, b, c] : span)
{
    do_thing(a, b, c);
}
```

# Comparison

Comparison is done on a row by row basis, where the row of one span is compared to the equivilently located row of another span.

# Operations

```CPP
int as[2]{1, 2};
char bs[2]{'4', '5'};
bool cs[2]{true, false};
Hz::NSpan span(as, bs, cs);
```
Span now contains:
```
[
  [1, 2],
  ['4', '5'],
  [true, false]
]
```

# Example

The vulkan function `vkQueuePresentKHR` takes a `VkPresentInfoKHR` create info structure, this structure has three fields, `swapchainCount`, `pSwapchains` and `pImageIndices`, the `swapchainCount` indicates the number of elements pointed to by the two `pSwapchains` and `pImageIndices` fields. This could be better expressed using `Hz::NSpan`:

```CPP
struct VkPresentInfoKHR
{
    ...
    uint32_t                 swapchainCount;
    const VkSwapchainKHR*    pSwapchains;
    const uint32_t*          pImageIndices;
    ...
};
```
vs:
```CPP
struct VkPresentInfoKHR
{
    ...
    Hz::NSpan<VkSwapchainKHR, uint32_t> swapchains_and_image_indices;
    ...
};
```
In this way, no documentation is needed to indicate that the number of swapchains and images must match. There is no reason to expect a performance penalty in this case, but maintainability should be improved.

# Bug Reporting

Please report bugs [here](https://gitlab.com/seppeon/nspan/-/issues).