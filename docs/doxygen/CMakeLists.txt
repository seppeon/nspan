find_package(Doxygen REQUIRED dot)
if (DOXYGEN_FOUND)
	include(FetchContent)
	FetchContent_Declare(
		DoxygenAwesome
		GIT_REPOSITORY	https://github.com/jothepro/doxygen-awesome-css.git
		GIT_TAG        	v2.2.1
	)
	FetchContent_MakeAvailable(DoxygenAwesome)

	set(DOXY_AWESOME ${doxygenawesome_SOURCE_DIR}/doxygen-awesome.css ${doxygenawesome_SOURCE_DIR}/doxygen-awesome-sidebar-only.css)
	set(DOXY_AWESOME_STRING "")
	foreach(DOXY_CSS ${DOXY_AWESOME})
		string(APPEND DOXY_AWESOME_STRING " \"${DOXY_CSS}\"")
	endforeach()

	set(DOXYGEN_HTML_EXTRA_STYLESHEET ${DOXY_AWESOME})
	set(DOXYGEN_HTML_COLORSTYLE LIGHT)
	set(DOXYGEN_FULL_SIDEBAR NO)
	set(DOXYGEN_DISABLE_INDEX NO)
	set(DOXYGEN_GENERATE_TREEVIEW YES)
	set(DOXYGEN_EXTRACT_STATIC YES)
	set(DOXYGEN_EXTRACT_PRIVATE NO)
	set(DOXYGEN_EXTRACT_ALL YES)
	set(DOXYGEN_PROJECT_NAME NSpan)
	set(DOXYGEN_DOT_IMAGE_FORMAT svg)
	set(DOXYGEN_SOURCE_BROWSER YES)
	set(DOXYGEN_ENABLE_PREPROCESSING YES)
	set(DOXYGEN_MACRO_EXPANSION YES)
	set(DOXYGEN_GENERATE_TODOLIST NO)
	set(DOXYGEN_EXPAND_ONLY_PREDEF YES)
	set(DOXYGEN_EXCLUDE_SYMBOLS "*Impl*")
	set(DOXYGEN_USE_MDFILE_AS_MAINPAGE ${CMAKE_CURRENT_SOURCE_DIR}/../../README.md)
	set(DOXYGEN_FILE_PATTERNS
		*.hpp
		*.cpp
		*.c
		*.h
		*.json
		*.md
		*.py
	)
	set(DOXYGEN_EXCLUDE_PATTERNS
		*conanfile.py*
		*CMake*.json
		*/build/*
		*/private/*
		*/test/*
		*/ci/*
		*/cmake/*
		*/src/*
		*/apps/*/include/*
		*/apps/*/src/*
		*/.vscode/*
		*/.intellisense/*
		*/Hz/NTup.hpp
	)
	doxygen_add_docs(
		docs
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../../
	)
else (DOXYGEN_FOUND)
	message("Doxygen need to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)