from conan import ConanFile
from conan.tools.build import check_min_cppstd
from conan.tools.files import load
from conan.tools.cmake import CMakeToolchain, CMakeDeps, CMake, cmake_layout
from conan.tools.build import can_run
import os
import re

required_conan_version = ">=1.50.0"

class NSpanRecipe(ConanFile):
    implements = ["auto_shared_fpic"]
    name = "hz_nspan"

    def set_version(self):
        content = load(self, os.path.join(self.recipe_folder, "CMakeLists.txt"))
        version = re.search("project\(NSpan VERSION (.*) LANGUAGES CXX\)", content).group(1)
        self.version = version.strip()

    # Optional metadata
    license = "MIT"
    author = "David Ledger davidledger@live.com.au"
    url = "https://gitlab.com/seppeon/nspan"
    homepage = "https://seppeon.gitlab.io/nspan/"
    description = "A library that allows viewing of multiple columns of data with a single index."
    topics = ("data oriented programming")
    requires = "catch2/3.4.0"
    build_policy = "missing"
    no_copy_source = True
    settings = "os", "compiler", "build_type", "arch"
    options = {
		"shared": [True, False],
		"fPIC": [True, False]
	}
    default_options = {
		"shared": False,
		"fPIC": True
	}

    exports_sources = "CMakeLists.txt", "README.md", "LICENSE.txt", "docs/doxygen/*", "src/*", "cmake/*", "test/*", "test_package/*", "include/*"
   
    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def layout(self):
        cmake_layout(self)

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, "20")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "NSpan")
        self.cpp_info.set_property("cmake_target_name", "Hz::NSpan")
        self.cpp_info.set_property("pkg_config_name", "NSpan")
        self.cpp_info.includedirs = ['include']
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []