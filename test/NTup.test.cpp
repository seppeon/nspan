#include "Hz/NTup.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("NTup is a comparable, accessable tuple", "[Hz::NTup]")
{
	using tup = Hz::NTup<const int, const char, const short>;
	REQUIRE(tup{1, 2, 3} == tup{1, 2, 3});
	REQUIRE_FALSE(tup{1, 2, 3} == tup{1, 2, 4});
	REQUIRE_FALSE(tup{1, 2, 3} == tup{1, 5, 3});
	REQUIRE_FALSE(tup{1, 2, 3} == tup{6, 2, 3});
	REQUIRE_FALSE(tup{1, 2, 3} == tup{2, 2, 3});
	REQUIRE_FALSE(tup{1, 2, 3} == tup{1, 3, 3});
	REQUIRE_FALSE(tup{1, 2, 3} == tup{1, 2, 4});
	REQUIRE_FALSE(tup{1, 2, 3} < tup{1, 2, 3});
	REQUIRE(tup{1, 2, 3} < tup{1, 2, 4});
	REQUIRE_FALSE(tup{1, 2, 4} < tup{1, 2, 3});
	REQUIRE(tup{1, 2, 3} < tup{1, 3, 4});
	REQUIRE(tup{1, 2, 4} < tup{1, 3, 3});
	REQUIRE_FALSE(tup{1, 2, 4} < tup{1, 1, 3});


	using ref_tup = Hz::NTup<int &, char &, short &>;
	int a = 0;
	char b = '1';
	short c = 2;
	int ae = 0;
	char be = '1';
	short ce = 2;
	REQUIRE(ref_tup{a, b, c} == ref_tup{ae, be, ce});
	++ce;
	REQUIRE_FALSE(ref_tup{a, b, c} == ref_tup{ae, be, ce});
	++be;
	REQUIRE_FALSE(ref_tup{a, b, c} == ref_tup{ae, be, ce});
	++ae;
	REQUIRE_FALSE(ref_tup{a, b, c} == ref_tup{ae, be, ce});

	using const_ref_tup = Hz::NTup<int const &, char const &, short const &>;
	const_ref_tup from = ref_tup{a, b, c};
	REQUIRE(get<0>(from) == a);
	REQUIRE(get<1>(from) == b);
	REQUIRE(get<2>(from) == c);
}