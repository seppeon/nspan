#include "Hz/Span.hpp"
#include <catch2/catch_all.hpp>
#include <vector>

namespace
{
	constexpr int a1[] = { 1, 4 } ;
	constexpr int a2[] = { 1, 5 } ;
	using Hz::Span;
}

TEST_CASE("Basic cases", "[Hz::Span]")
{
	STATIC_REQUIRE(Hz::Std::HasData<decltype(a1), Hz::ConvertibleToAnyPointer>);
	auto test0 = Span{a1};
	auto test1 = Span{a2};
	REQUIRE(test0.Data() == &a1[0]);
	REQUIRE(test0 == test0);
	REQUIRE_FALSE(test0 == test1);
	REQUIRE_FALSE(test0.Empty());
	REQUIRE(test0.Size() == 2);
	REQUIRE_FALSE(test0 != test0);
	REQUIRE_FALSE(test0 < test0);
	REQUIRE_FALSE(test0 > test0);
	REQUIRE(test0 <= test0);
	REQUIRE(test0 >= test0);
	REQUIRE(test0.Front() == a1[0]);
	REQUIRE(test0[0] == a1[0]);
	REQUIRE(test0.Back() == a1[1]);
	REQUIRE(test0[1] == a1[1]);

	for (std::size_t v = 0; v < test0.Size(); ++v)
	{
		REQUIRE(test0[v] == a1[v]);
	} 

	auto popped_front = test0.PopFront();
	REQUIRE(popped_front.Size() == 1);
	REQUIRE(popped_front.Front() == test0[1]);
	REQUIRE(popped_front != test0);

	auto popped_back = test0.PopBack();
	REQUIRE(popped_back.Size() == 1);
	REQUIRE(popped_back.Front() == test0[0]);

	auto popped_back_excessive = test0.PopBack(100);
	REQUIRE(popped_back_excessive.Empty());

	auto popped_front_excessive = test0.PopFront(100);
	REQUIRE(popped_front_excessive.Empty());

	auto left_subview = test0.Subspan(0, 1);
	REQUIRE(left_subview == popped_back);

	auto right_subview = test0.Subspan(1, 1);
	REQUIRE(right_subview == popped_front);

	auto first = test0.First(1);
	REQUIRE(first == popped_back);

	auto last = test0.Last(1);
	REQUIRE(last == popped_front);

	auto [split_left, split_right] = test0.Split(1);
	REQUIRE(split_left == first);
	REQUIRE(split_right == last);

	auto pointer_construction = Span(2, &a1[0]);
	REQUIRE(pointer_construction == test0);
}

TEST_CASE("Random access iteration", "[Hz::Span]")
{
	int const a[]{1, 2, 3};
	char const b[]{'4', '5', '6'};
	short const c[]{7, 8, 9};

	auto test = Span(a);
	auto it = test.begin();
	auto const end = test.end();
	REQUIRE(it != end);

	// Do not change this to std::difference, it has an undefined implementation
	// where-as range based for is well defined.
	std::size_t count = 0;
	for (auto _ : test) { ++count; }
	REQUIRE(count == 3);
	REQUIRE(*it == a[0]);
	REQUIRE(it[0] == a[0]);
	REQUIRE(it[1] == a[1]);

	REQUIRE(it == it);
	REQUIRE_FALSE(it != it);
	REQUIRE_FALSE(it < it);
	REQUIRE_FALSE(it > it);
	REQUIRE(it <= it);
	REQUIRE(it >= it);

	auto curr = it++;
	REQUIRE_FALSE(curr == it);
	REQUIRE(curr != it);
	REQUIRE_FALSE(it < curr);
	REQUIRE(curr < it);
	REQUIRE_FALSE(curr > it);
	REQUIRE(it > curr);
	REQUIRE(curr <= it);
	REQUIRE_FALSE(it <= curr);
	REQUIRE_FALSE(curr >= it);
	REQUIRE(it >= curr);
	REQUIRE(curr != it);
	REQUIRE(it > curr);
	REQUIRE(*curr == a[0]);
	REQUIRE(curr[0] == a[0]);
	REQUIRE(*it == a[1]);
	REQUIRE(it[0] == a[1]);
	REQUIRE(it[1] == a[2]);
	REQUIRE(it - 1 == curr);
	REQUIRE(curr + 1 == it);
	REQUIRE(1 + curr == it);

	auto old = it;
	auto next = it--;
	REQUIRE(next == old);
	REQUIRE(it == curr);

	auto delta_2 = it += 2;
	REQUIRE(delta_2 == it);
	REQUIRE(*it == a[2]);

	auto de_delta_2 = it -= 2;
	REQUIRE(de_delta_2 == it);
	REQUIRE(*it == a[0]);
}