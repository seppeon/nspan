#include "Hz/NSpan.hpp"
#include <catch2/catch_all.hpp>

namespace
{
	constexpr int   a1[] = { 1, 4 } ;
	constexpr char  b1[] = { '2', '5' };
	constexpr short c1[] = { 3, 6 };
	constexpr int   a2[] = { 1, 5 } ;
	constexpr char  b2[] = { '2', '6' };
	constexpr short c2[] = { 3, 7 };
	using Hz::NSpan;
}

TEST_CASE("Basic cases", "[Hz::NSpan]")
{
	using tup = Hz::NTup<const int, const char, const short>;

	auto test0 = NSpan{a1, b1, c1};
	auto test1 = NSpan{a1, b1, c2};
	auto test2 = NSpan{a1, b2, c2};
	auto test3 = NSpan{a2, b2, c2};

	REQUIRE(test0 == test0);
	REQUIRE_FALSE(test0 == test1);
	REQUIRE_FALSE(test0 == test2);
	REQUIRE_FALSE(test0 == test3);
	REQUIRE_FALSE(test0.Empty());
	REQUIRE(test0.Columns() == 3);
	REQUIRE(test0.Size() == 2);
	REQUIRE_FALSE(test0 != test0);
	REQUIRE_FALSE(test0 < test0);
	REQUIRE_FALSE(test0 > test0);
	REQUIRE(test0 <= test0);
	REQUIRE(test0 >= test0);
	REQUIRE(test0.Front() == tup{a1[0], b1[0], c1[0]});
	REQUIRE(test0[0] == tup{a1[0], b1[0], c1[0]});
	REQUIRE(test0.Back() == tup{a1[1], b1[1], c1[1]});
	REQUIRE(test0[1] == tup{a1[1], b1[1], c1[1]});

	for (std::size_t v = 0; v < test0.Size(); ++v)
	{
		Hz::apply([v](int av, char bv, short cv)
		{
			REQUIRE(av == a1[v]);
			REQUIRE(bv == b1[v]);
			REQUIRE(cv == c1[v]);
		}, test0[v]);
	} 

	auto popped_front = test0.PopFront();
	REQUIRE(popped_front.Size() == 1);
	REQUIRE(popped_front.Columns() == test0.Columns());
	REQUIRE(popped_front.Front() == test0[1]);
	REQUIRE(popped_front != test0);

	auto popped_back = test0.PopBack();
	REQUIRE(popped_back.Size() == 1);
	REQUIRE(popped_back.Columns() == test0.Columns());
	REQUIRE(popped_back.Front() == test0[0]);

	auto popped_back_excessive = test0.PopBack(100);
	REQUIRE(popped_back_excessive.Empty());

	auto popped_front_excessive = test0.PopFront(100);
	REQUIRE(popped_front_excessive.Empty());

	auto left_subview = test0.Subspan(0, 1);
	REQUIRE(left_subview == popped_back);

	auto right_subview = test0.Subspan(1, 1);
	REQUIRE(right_subview == popped_front);

	auto first = test0.First(1);
	REQUIRE(first == popped_back);

	auto last = test0.Last(1);
	REQUIRE(last == popped_front);

	auto [split_left, split_right] = test0.Split(1);
	REQUIRE(split_left == first);
	REQUIRE(split_right == last);

	auto pointer_construction = NSpan(2, &a1[0], &b1[0], &c1[0]);
	REQUIRE(pointer_construction == test0);
}

TEST_CASE("Random access iteration", "[Hz::NSpan]")
{
	using tup = Hz::NTup<int const &, char const &, short const &>;

	int const a[]{1, 2, 3};
	char const b[]{'4', '5', '6'};
	short const c[]{7, 8, 9};

	auto test = NSpan(a, b, c);
	auto it = test.begin();
	auto const end = test.end();
	REQUIRE(it != end);
	auto ptrs = test.Data();
	REQUIRE(get<0>(ptrs) == std::data(a));
	REQUIRE(get<1>(ptrs) == std::data(b));
	REQUIRE(get<2>(ptrs) == std::data(c));

	// Do not change this to std::difference, it has an undefined implementation
	// where-as range based for is well defined.
	std::size_t count = 0;
	for (auto _ : test) { ++count; }
	REQUIRE(count == 3);
	REQUIRE(*it == tup{ a[0], b[0], c[0] });
	REQUIRE(it[0] == tup{ a[0], b[0], c[0] });
	REQUIRE(it[1] == tup{ a[1], b[1], c[1] });

	REQUIRE(it == it);
	REQUIRE_FALSE(it != it);
	REQUIRE_FALSE(it < it);
	REQUIRE_FALSE(it > it);
	REQUIRE(it <= it);
	REQUIRE(it >= it);

	auto curr = it++;
	REQUIRE_FALSE(curr == it);
	REQUIRE(curr != it);
	REQUIRE_FALSE(it < curr);
	REQUIRE(curr < it);
	REQUIRE_FALSE(curr > it);
	REQUIRE(it > curr);
	REQUIRE(curr <= it);
	REQUIRE_FALSE(it <= curr);
	REQUIRE_FALSE(curr >= it);
	REQUIRE(it >= curr);
	REQUIRE(curr != it);
	REQUIRE(it > curr);
	REQUIRE(*curr == tup{ a[0], b[0], c[0] });
	REQUIRE(curr[0] == tup{ a[0], b[0], c[0] });
	REQUIRE(*it == tup{ a[1], b[1], c[1] });
	REQUIRE(it[0] == tup{ a[1], b[1], c[1] });
	REQUIRE(it[1] == tup{ a[2], b[2], c[2] });
	REQUIRE(it - 1 == curr);
	REQUIRE(curr + 1 == it);
	REQUIRE(1 + curr == it);

	auto old = it;
	auto next = it--;
	REQUIRE(next == old);
	REQUIRE(it == curr);

	auto delta_2 = it += 2;
	REQUIRE(delta_2 == it);
	REQUIRE(*it == tup{a[2], b[2], c[2]});

	auto de_delta_2 = it -= 2;
	REQUIRE(de_delta_2 == it);
	REQUIRE(*it == tup{a[0], b[0], c[0]});
}