cmake_minimum_required(VERSION 3.22)
project(NSpan VERSION 0.2.3 LANGUAGES CXX)

option(NSPAN_TESTS_ENABLED 	"Are tests ON or OFF?" 						ON)
option(NSPAN_CODE_COVERAGE 	"Enable code coverage report generation?" 	OFF)
option(NSPAN_BUILD_DOC      "Build documentation"                       ON)

if (NSPAN_TESTS_ENABLED)
	add_subdirectory(test)
endif()
if (NSPAN_BUILD_DOC)
	add_subdirectory(docs/doxygen)
endif()

# Its simply an interface target, so nothing weird.
add_library(NSpan INTERFACE)
add_library(Hz::NSpan ALIAS NSpan)
target_compile_definitions(NSpan INTERFACE NSPAN_LIB_VERSION="${CMAKE_PROJECT_VERSION}")
target_compile_definitions(NSpan INTERFACE NSPAN_LIB_MAJOR_VERSION=${CMAKE_PROJECT_VERSION_MAJOR})
target_compile_definitions(NSpan INTERFACE NSPAN_LIB_MINOR_VERSION=${CMAKE_PROJECT_VERSION_MINOR})
target_compile_definitions(NSpan INTERFACE NSPAN_LIB_PATCH_VERSION=${CMAKE_PROJECT_VERSION_PATCH})
target_include_directories(NSpan
	INTERFACE
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)

if (NSPAN_CODE_COVERAGE AND NSPAN_TESTS_ENABLED)
	find_program(GCOV_PATH gcov)
	find_program(GCOVR_PATH gcovr)
	if (NOT MSVC)
		if (GCOV_PATH AND GCOVR_PATH)
			target_compile_options(NSpan_test PUBLIC -fprofile-arcs -ftest-coverage)
			target_link_options(NSpan_test PUBLIC -lgcov --coverage)

			add_custom_target(NSpan_Coverage
				COMMAND $<TARGET_FILE:NSpan_test>
				COMMAND
					${GCOVR_PATH}
						--root "${CMAKE_CURRENT_SOURCE_DIR}"
						--exclude "${CMAKE_CURRENT_SOURCE_DIR}/test"
						--html-details
						-o "$<TARGET_FILE_DIR:NSpan_test>/output.html"
				COMMENT "GCOVR code coverage info report saved in '$<TARGET_FILE_DIR:NSpan_test>'."
			)
			add_dependencies(NSpan_Coverage NSpan NSpan_test)
		endif()
	endif()
endif()

include(GNUInstallDirs)
include(cmake/install.cmake)
include(cmake/cpack.cmake)