include(CMakePackageConfigHelpers)

# Avoid conflicting debug and release binaries
set(CMAKE_DEBUG_POSTFIX d)

# Create the install
set(NAMESPACE Hz)
set(PACKAGE_NAME NSpan)
set(CONFIG_FILE_TEMPLATE ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Config.cmake.in)
set(CONFIG_FILE ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE_NAME}Config.cmake)
set(VERSION_FILE ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE_NAME}ConfigVersion.cmake)
set(CONFIG_FILE_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/cmake)

# generate the config file that includes the exports
configure_package_config_file(
	# Input
		"${CONFIG_FILE_TEMPLATE}"
	# Output
		"${CONFIG_FILE}"
	INSTALL_DESTINATION
		"${CONFIG_FILE_INSTALL_DIR}"
)
write_basic_package_version_file(
	"${VERSION_FILE}"
	VERSION "${PROJECT_VERSION}"
	COMPATIBILITY SameMajorVersion
)
install(
	TARGETS ${PACKAGE_NAME}
	EXPORT ${PACKAGE_NAME}Targets
)
install(
	DIRECTORY "include/${NAMESPACE}"
	TYPE INCLUDE
)
install(
	EXPORT ${PACKAGE_NAME}Targets
	FILE ${PACKAGE_NAME}Targets.cmake
	DESTINATION "${CONFIG_FILE_INSTALL_DIR}"
	NAMESPACE ${NAMESPACE}::
)
install(
	FILES
		"${CONFIG_FILE}"
		"${VERSION_FILE}"
	DESTINATION
		"${CONFIG_FILE_INSTALL_DIR}"
)
